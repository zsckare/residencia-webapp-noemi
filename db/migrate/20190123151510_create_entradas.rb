class CreateEntradas < ActiveRecord::Migration[5.2]
  def change
    create_table :entradas do |t|
      t.string :name
      t.string :description
      t.string :price
      t.string :qty
      t.string :payment

      t.timestamps
    end
  end
end
