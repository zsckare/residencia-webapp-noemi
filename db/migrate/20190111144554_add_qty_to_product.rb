class AddQtyToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :qty, :integer
  end
end
