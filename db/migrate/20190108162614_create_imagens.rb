class CreateImagens < ActiveRecord::Migration[5.2]
  def change
    create_table :imagens do |t|
      t.string :name
      t.references :gallery, foreign_key: true
      t.boolean :active, default: true
      t.timestamps
    end
  end
end
