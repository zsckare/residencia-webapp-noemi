class CreateSalidas < ActiveRecord::Migration[5.2]
  def change
    create_table :salidas do |t|
      t.string :name
      t.string :description
      t.string :price
      t.string :qty
      t.string :payment

      t.timestamps
    end
  end
end
