json.extract! provider, :id, :name, :phone, :directin, :email, :created_at, :updated_at
json.url provider_url(provider, format: :json)
