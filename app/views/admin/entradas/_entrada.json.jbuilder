json.extract! entrada, :id, :name, :description, :price, :qty, :payment, :created_at, :updated_at
json.url entrada_url(entrada, format: :json)
