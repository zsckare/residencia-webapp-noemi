json.extract! salida, :id, :name, :description, :price, :qty, :payment, :created_at, :updated_at
json.url salida_url(salida, format: :json)
