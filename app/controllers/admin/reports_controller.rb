class Admin::ReportsController < Admin::BaseController
  skip_before_action :verify_authenticity_token
  def index
    @reports = Report.all
    @products = Product.all
  end

  def save

    datos = {
      products:params[:datos]
    }

    d = Time.now.strftime("%d/%m/%Y %H:%M")
    r = Report.new
    r.fecha = d
    r.datos = datos.to_json
    if r.save
    render json: {status:200, results:r}
    else
      render json: {status:500}
    end
  end
end
