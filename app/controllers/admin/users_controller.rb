class Admin::UsersController < Admin::BaseController
  before_action :set_provider, only: [:show, :edit, :update, :destroy]

 
  def index
    @users = User.all
  end

  def show
  end

  
  def new
    @user = User.new
    @action = "Crear"
  end

  
  def edit
    @action = "Cuardar Cambios"
  end


  def create
    @user = User.new(provider_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_users_path, notice: 'Provider was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    respond_to do |format|
      if @user.update(provider_params)
        format.html { redirect_to admin_users_path, notice: 'Provider was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_path, notice: 'Provider was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_provider
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def provider_params
      params.require(:user).permit(:email, :password, :level)
    end
end
