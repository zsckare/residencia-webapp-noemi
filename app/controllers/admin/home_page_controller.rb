
class Admin::HomePageController <  Admin::BaseController
  skip_before_action :verify_authenticity_token
  def index
  end

  def main_gallery
    @items = []
    
    @images = Imagen.where(gallery_id: 1)
    @images.each do |imge|
      puts imge.img.service_url
      item = {
        id: imge.id,
        url: imge.img.service_url,
        active: imge.active
      }
      @items.push(item)
    end
  end

  def upload_photos_main_gallery


    image = Imagen.new
    image.img = params[:fileToUpload]
    image.gallery_id = 1
    if image.save
    redirect_to admin_editar_galeria_url
    else
      puts image.errors
    end
  end

  def delete_img
    img = Imagen.find(params[:id])

    if img.destroy
      @items = []
    
      @images = Imagen.where(gallery_id: 1)
      @images.each do |imge|
        puts imge.img.service_url
        item = {
          id: imge.id,
          url: imge.img.service_url,
          active: imge.active
        }
        @items.push(item)
      end
      render json: {status: 200, results: @items}
    else
      render json: {status: 100}
    end
  end
end
