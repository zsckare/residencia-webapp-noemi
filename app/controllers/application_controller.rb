class ApplicationController < ActionController::Base
    layout :layout_by_resource
    before_action :set_host_for_local_storage
    
    private
     def set_host_for_local_storage
        ActiveStorage::Current.host = request.base_url if Rails.application.config.active_storage.service == :local
    end


  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end
end
