class Imagen < ApplicationRecord
  belongs_to :gallery
  has_one_attached :img, dependent: :destroy
end
