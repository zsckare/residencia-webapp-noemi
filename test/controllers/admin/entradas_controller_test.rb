require 'test_helper'

class Admin::EntradasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_entrada = admin_entradas(:one)
  end

  test "should get index" do
    get admin_entradas_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_entrada_url
    assert_response :success
  end

  test "should create admin_entrada" do
    assert_difference('Admin::Entrada.count') do
      post admin_entradas_url, params: { admin_entrada: {  } }
    end

    assert_redirected_to admin_entrada_url(Admin::Entrada.last)
  end

  test "should show admin_entrada" do
    get admin_entrada_url(@admin_entrada)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_entrada_url(@admin_entrada)
    assert_response :success
  end

  test "should update admin_entrada" do
    patch admin_entrada_url(@admin_entrada), params: { admin_entrada: {  } }
    assert_redirected_to admin_entrada_url(@admin_entrada)
  end

  test "should destroy admin_entrada" do
    assert_difference('Admin::Entrada.count', -1) do
      delete admin_entrada_url(@admin_entrada)
    end

    assert_redirected_to admin_entradas_url
  end
end
