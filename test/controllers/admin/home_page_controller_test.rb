require 'test_helper'

class Admin::HomePageControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_home_page_index_url
    assert_response :success
  end

end
