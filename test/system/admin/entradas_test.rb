require "application_system_test_case"

class Admin::EntradasTest < ApplicationSystemTestCase
  setup do
    @admin_entrada = admin_entradas(:one)
  end

  test "visiting the index" do
    visit admin_entradas_url
    assert_selector "h1", text: "Admin/Entradas"
  end

  test "creating a Entrada" do
    visit admin_entradas_url
    click_on "New Admin/Entrada"

    click_on "Create Entrada"

    assert_text "Entrada was successfully created"
    click_on "Back"
  end

  test "updating a Entrada" do
    visit admin_entradas_url
    click_on "Edit", match: :first

    click_on "Update Entrada"

    assert_text "Entrada was successfully updated"
    click_on "Back"
  end

  test "destroying a Entrada" do
    visit admin_entradas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Entrada was successfully destroyed"
  end
end
