Rails.application.routes.draw do
  
  


  devise_for :users
devise_scope :user do
  get '/users/sign_out' => 'devise/sessions#destroy'
end
  namespace :admin do
    root 'products#index'
    resources :services
    resources :projects
    resources :products
    resources :providers
    resources :entradas
    resources :users     
    resources :salidas
    get "/pagina-inicio", to: "home_page#index"
    get "/editar-galeria", to: "home_page#main_gallery"
    post "/post-galeria", to: "home_page#upload_photos_main_gallery"
    post "/delete-img", to: "home_page#delete_img"
    get "/reportes", to: "reports#index"
    post "/reports/save", to: "reports#save"
  end
  root 'welcome#index'
  resources :services
  resources :projects
  resources :products
  resources :providers
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
